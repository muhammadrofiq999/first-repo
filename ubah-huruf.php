<?php
function ubah_huruf($string)
{
    $abjad = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
    $output = "";
    if (!is_string($string)) {
        return "Masukkan String";
    } else {
        foreach (str_split($string) as $str) {
            for ($i = 0; $i < count($abjad); $i++) {
                if (strtolower($str) == $abjad[$i]) {
                    $output .= $abjad[$i + 1];
                }
            }
        }
    }
    return $output . "<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
