<?php
function tentukan_nilai($number)
{
    $output = "";
    if ($number <= 100 && $number >= 85) {
        $output .= "Sangat Baik";
    } elseif ($number < 85 && $number >= 70) {
        $output .= "Baik";
    } elseif ($number < 70 && $number >= 60) {
        $output .= "Cukup";
    } elseif ($number < 60) {
        $output .= "Kurang";
    }
    $output .= "<br>";
    return $output;
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
